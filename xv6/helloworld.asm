
_helloworld:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"
#include "stat.h"


int main(int argc, char** argv){
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	83 ec 10             	sub    $0x10,%esp
	printf(1, "%s", "Hello, world!\n");
   9:	c7 44 24 08 d3 07 00 	movl   $0x7d3,0x8(%esp)
  10:	00 
  11:	c7 44 24 04 e2 07 00 	movl   $0x7e2,0x4(%esp)
  18:	00 
  19:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  20:	e8 e7 03 00 00       	call   40c <printf>
	exit();
  25:	e8 66 02 00 00       	call   290 <exit>
  2a:	90                   	nop
  2b:	90                   	nop

0000002c <stosb>:
  2c:	55                   	push   %ebp
  2d:	89 e5                	mov    %esp,%ebp
  2f:	57                   	push   %edi
  30:	53                   	push   %ebx
  31:	8b 4d 08             	mov    0x8(%ebp),%ecx
  34:	8b 55 10             	mov    0x10(%ebp),%edx
  37:	8b 45 0c             	mov    0xc(%ebp),%eax
  3a:	89 cb                	mov    %ecx,%ebx
  3c:	89 df                	mov    %ebx,%edi
  3e:	89 d1                	mov    %edx,%ecx
  40:	fc                   	cld    
  41:	f3 aa                	rep stos %al,%es:(%edi)
  43:	89 ca                	mov    %ecx,%edx
  45:	89 fb                	mov    %edi,%ebx
  47:	89 5d 08             	mov    %ebx,0x8(%ebp)
  4a:	89 55 10             	mov    %edx,0x10(%ebp)
  4d:	5b                   	pop    %ebx
  4e:	5f                   	pop    %edi
  4f:	5d                   	pop    %ebp
  50:	c3                   	ret    

00000051 <strcpy>:
  51:	55                   	push   %ebp
  52:	89 e5                	mov    %esp,%ebp
  54:	83 ec 10             	sub    $0x10,%esp
  57:	8b 45 08             	mov    0x8(%ebp),%eax
  5a:	89 45 fc             	mov    %eax,-0x4(%ebp)
  5d:	8b 45 0c             	mov    0xc(%ebp),%eax
  60:	0f b6 10             	movzbl (%eax),%edx
  63:	8b 45 08             	mov    0x8(%ebp),%eax
  66:	88 10                	mov    %dl,(%eax)
  68:	8b 45 08             	mov    0x8(%ebp),%eax
  6b:	0f b6 00             	movzbl (%eax),%eax
  6e:	84 c0                	test   %al,%al
  70:	0f 95 c0             	setne  %al
  73:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  77:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  7b:	84 c0                	test   %al,%al
  7d:	75 de                	jne    5d <strcpy+0xc>
  7f:	8b 45 fc             	mov    -0x4(%ebp),%eax
  82:	c9                   	leave  
  83:	c3                   	ret    

00000084 <strcmp>:
  84:	55                   	push   %ebp
  85:	89 e5                	mov    %esp,%ebp
  87:	eb 08                	jmp    91 <strcmp+0xd>
  89:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  91:	8b 45 08             	mov    0x8(%ebp),%eax
  94:	0f b6 00             	movzbl (%eax),%eax
  97:	84 c0                	test   %al,%al
  99:	74 10                	je     ab <strcmp+0x27>
  9b:	8b 45 08             	mov    0x8(%ebp),%eax
  9e:	0f b6 10             	movzbl (%eax),%edx
  a1:	8b 45 0c             	mov    0xc(%ebp),%eax
  a4:	0f b6 00             	movzbl (%eax),%eax
  a7:	38 c2                	cmp    %al,%dl
  a9:	74 de                	je     89 <strcmp+0x5>
  ab:	8b 45 08             	mov    0x8(%ebp),%eax
  ae:	0f b6 00             	movzbl (%eax),%eax
  b1:	0f b6 d0             	movzbl %al,%edx
  b4:	8b 45 0c             	mov    0xc(%ebp),%eax
  b7:	0f b6 00             	movzbl (%eax),%eax
  ba:	0f b6 c0             	movzbl %al,%eax
  bd:	89 d1                	mov    %edx,%ecx
  bf:	29 c1                	sub    %eax,%ecx
  c1:	89 c8                	mov    %ecx,%eax
  c3:	5d                   	pop    %ebp
  c4:	c3                   	ret    

000000c5 <strlen>:
  c5:	55                   	push   %ebp
  c6:	89 e5                	mov    %esp,%ebp
  c8:	83 ec 10             	sub    $0x10,%esp
  cb:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  d2:	eb 04                	jmp    d8 <strlen+0x13>
  d4:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  d8:	8b 45 fc             	mov    -0x4(%ebp),%eax
  db:	03 45 08             	add    0x8(%ebp),%eax
  de:	0f b6 00             	movzbl (%eax),%eax
  e1:	84 c0                	test   %al,%al
  e3:	75 ef                	jne    d4 <strlen+0xf>
  e5:	8b 45 fc             	mov    -0x4(%ebp),%eax
  e8:	c9                   	leave  
  e9:	c3                   	ret    

000000ea <memset>:
  ea:	55                   	push   %ebp
  eb:	89 e5                	mov    %esp,%ebp
  ed:	83 ec 0c             	sub    $0xc,%esp
  f0:	8b 45 10             	mov    0x10(%ebp),%eax
  f3:	89 44 24 08          	mov    %eax,0x8(%esp)
  f7:	8b 45 0c             	mov    0xc(%ebp),%eax
  fa:	89 44 24 04          	mov    %eax,0x4(%esp)
  fe:	8b 45 08             	mov    0x8(%ebp),%eax
 101:	89 04 24             	mov    %eax,(%esp)
 104:	e8 23 ff ff ff       	call   2c <stosb>
 109:	8b 45 08             	mov    0x8(%ebp),%eax
 10c:	c9                   	leave  
 10d:	c3                   	ret    

0000010e <strchr>:
 10e:	55                   	push   %ebp
 10f:	89 e5                	mov    %esp,%ebp
 111:	83 ec 04             	sub    $0x4,%esp
 114:	8b 45 0c             	mov    0xc(%ebp),%eax
 117:	88 45 fc             	mov    %al,-0x4(%ebp)
 11a:	eb 14                	jmp    130 <strchr+0x22>
 11c:	8b 45 08             	mov    0x8(%ebp),%eax
 11f:	0f b6 00             	movzbl (%eax),%eax
 122:	3a 45 fc             	cmp    -0x4(%ebp),%al
 125:	75 05                	jne    12c <strchr+0x1e>
 127:	8b 45 08             	mov    0x8(%ebp),%eax
 12a:	eb 13                	jmp    13f <strchr+0x31>
 12c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 130:	8b 45 08             	mov    0x8(%ebp),%eax
 133:	0f b6 00             	movzbl (%eax),%eax
 136:	84 c0                	test   %al,%al
 138:	75 e2                	jne    11c <strchr+0xe>
 13a:	b8 00 00 00 00       	mov    $0x0,%eax
 13f:	c9                   	leave  
 140:	c3                   	ret    

00000141 <gets>:
 141:	55                   	push   %ebp
 142:	89 e5                	mov    %esp,%ebp
 144:	83 ec 28             	sub    $0x28,%esp
 147:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 14e:	eb 44                	jmp    194 <gets+0x53>
 150:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 157:	00 
 158:	8d 45 ef             	lea    -0x11(%ebp),%eax
 15b:	89 44 24 04          	mov    %eax,0x4(%esp)
 15f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 166:	e8 3d 01 00 00       	call   2a8 <read>
 16b:	89 45 f4             	mov    %eax,-0xc(%ebp)
 16e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 172:	7e 2d                	jle    1a1 <gets+0x60>
 174:	8b 45 f0             	mov    -0x10(%ebp),%eax
 177:	03 45 08             	add    0x8(%ebp),%eax
 17a:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 17e:	88 10                	mov    %dl,(%eax)
 180:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 184:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 188:	3c 0a                	cmp    $0xa,%al
 18a:	74 16                	je     1a2 <gets+0x61>
 18c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 190:	3c 0d                	cmp    $0xd,%al
 192:	74 0e                	je     1a2 <gets+0x61>
 194:	8b 45 f0             	mov    -0x10(%ebp),%eax
 197:	83 c0 01             	add    $0x1,%eax
 19a:	3b 45 0c             	cmp    0xc(%ebp),%eax
 19d:	7c b1                	jl     150 <gets+0xf>
 19f:	eb 01                	jmp    1a2 <gets+0x61>
 1a1:	90                   	nop
 1a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1a5:	03 45 08             	add    0x8(%ebp),%eax
 1a8:	c6 00 00             	movb   $0x0,(%eax)
 1ab:	8b 45 08             	mov    0x8(%ebp),%eax
 1ae:	c9                   	leave  
 1af:	c3                   	ret    

000001b0 <stat>:
 1b0:	55                   	push   %ebp
 1b1:	89 e5                	mov    %esp,%ebp
 1b3:	83 ec 28             	sub    $0x28,%esp
 1b6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 1bd:	00 
 1be:	8b 45 08             	mov    0x8(%ebp),%eax
 1c1:	89 04 24             	mov    %eax,(%esp)
 1c4:	e8 07 01 00 00       	call   2d0 <open>
 1c9:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1cc:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1d0:	79 07                	jns    1d9 <stat+0x29>
 1d2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1d7:	eb 23                	jmp    1fc <stat+0x4c>
 1d9:	8b 45 0c             	mov    0xc(%ebp),%eax
 1dc:	89 44 24 04          	mov    %eax,0x4(%esp)
 1e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1e3:	89 04 24             	mov    %eax,(%esp)
 1e6:	e8 fd 00 00 00       	call   2e8 <fstat>
 1eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
 1ee:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1f1:	89 04 24             	mov    %eax,(%esp)
 1f4:	e8 bf 00 00 00       	call   2b8 <close>
 1f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1fc:	c9                   	leave  
 1fd:	c3                   	ret    

000001fe <atoi>:
 1fe:	55                   	push   %ebp
 1ff:	89 e5                	mov    %esp,%ebp
 201:	83 ec 10             	sub    $0x10,%esp
 204:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 20b:	eb 24                	jmp    231 <atoi+0x33>
 20d:	8b 55 fc             	mov    -0x4(%ebp),%edx
 210:	89 d0                	mov    %edx,%eax
 212:	c1 e0 02             	shl    $0x2,%eax
 215:	01 d0                	add    %edx,%eax
 217:	01 c0                	add    %eax,%eax
 219:	89 c2                	mov    %eax,%edx
 21b:	8b 45 08             	mov    0x8(%ebp),%eax
 21e:	0f b6 00             	movzbl (%eax),%eax
 221:	0f be c0             	movsbl %al,%eax
 224:	8d 04 02             	lea    (%edx,%eax,1),%eax
 227:	83 e8 30             	sub    $0x30,%eax
 22a:	89 45 fc             	mov    %eax,-0x4(%ebp)
 22d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 231:	8b 45 08             	mov    0x8(%ebp),%eax
 234:	0f b6 00             	movzbl (%eax),%eax
 237:	3c 2f                	cmp    $0x2f,%al
 239:	7e 0a                	jle    245 <atoi+0x47>
 23b:	8b 45 08             	mov    0x8(%ebp),%eax
 23e:	0f b6 00             	movzbl (%eax),%eax
 241:	3c 39                	cmp    $0x39,%al
 243:	7e c8                	jle    20d <atoi+0xf>
 245:	8b 45 fc             	mov    -0x4(%ebp),%eax
 248:	c9                   	leave  
 249:	c3                   	ret    

0000024a <memmove>:
 24a:	55                   	push   %ebp
 24b:	89 e5                	mov    %esp,%ebp
 24d:	83 ec 10             	sub    $0x10,%esp
 250:	8b 45 08             	mov    0x8(%ebp),%eax
 253:	89 45 f8             	mov    %eax,-0x8(%ebp)
 256:	8b 45 0c             	mov    0xc(%ebp),%eax
 259:	89 45 fc             	mov    %eax,-0x4(%ebp)
 25c:	eb 13                	jmp    271 <memmove+0x27>
 25e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 261:	0f b6 10             	movzbl (%eax),%edx
 264:	8b 45 f8             	mov    -0x8(%ebp),%eax
 267:	88 10                	mov    %dl,(%eax)
 269:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 26d:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 271:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 275:	0f 9f c0             	setg   %al
 278:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 27c:	84 c0                	test   %al,%al
 27e:	75 de                	jne    25e <memmove+0x14>
 280:	8b 45 08             	mov    0x8(%ebp),%eax
 283:	c9                   	leave  
 284:	c3                   	ret    
 285:	90                   	nop
 286:	90                   	nop
 287:	90                   	nop

00000288 <fork>:
 288:	b8 01 00 00 00       	mov    $0x1,%eax
 28d:	cd 40                	int    $0x40
 28f:	c3                   	ret    

00000290 <exit>:
 290:	b8 02 00 00 00       	mov    $0x2,%eax
 295:	cd 40                	int    $0x40
 297:	c3                   	ret    

00000298 <wait>:
 298:	b8 03 00 00 00       	mov    $0x3,%eax
 29d:	cd 40                	int    $0x40
 29f:	c3                   	ret    

000002a0 <pipe>:
 2a0:	b8 04 00 00 00       	mov    $0x4,%eax
 2a5:	cd 40                	int    $0x40
 2a7:	c3                   	ret    

000002a8 <read>:
 2a8:	b8 05 00 00 00       	mov    $0x5,%eax
 2ad:	cd 40                	int    $0x40
 2af:	c3                   	ret    

000002b0 <write>:
 2b0:	b8 10 00 00 00       	mov    $0x10,%eax
 2b5:	cd 40                	int    $0x40
 2b7:	c3                   	ret    

000002b8 <close>:
 2b8:	b8 15 00 00 00       	mov    $0x15,%eax
 2bd:	cd 40                	int    $0x40
 2bf:	c3                   	ret    

000002c0 <kill>:
 2c0:	b8 06 00 00 00       	mov    $0x6,%eax
 2c5:	cd 40                	int    $0x40
 2c7:	c3                   	ret    

000002c8 <exec>:
 2c8:	b8 07 00 00 00       	mov    $0x7,%eax
 2cd:	cd 40                	int    $0x40
 2cf:	c3                   	ret    

000002d0 <open>:
 2d0:	b8 0f 00 00 00       	mov    $0xf,%eax
 2d5:	cd 40                	int    $0x40
 2d7:	c3                   	ret    

000002d8 <mknod>:
 2d8:	b8 11 00 00 00       	mov    $0x11,%eax
 2dd:	cd 40                	int    $0x40
 2df:	c3                   	ret    

000002e0 <unlink>:
 2e0:	b8 12 00 00 00       	mov    $0x12,%eax
 2e5:	cd 40                	int    $0x40
 2e7:	c3                   	ret    

000002e8 <fstat>:
 2e8:	b8 08 00 00 00       	mov    $0x8,%eax
 2ed:	cd 40                	int    $0x40
 2ef:	c3                   	ret    

000002f0 <link>:
 2f0:	b8 13 00 00 00       	mov    $0x13,%eax
 2f5:	cd 40                	int    $0x40
 2f7:	c3                   	ret    

000002f8 <mkdir>:
 2f8:	b8 14 00 00 00       	mov    $0x14,%eax
 2fd:	cd 40                	int    $0x40
 2ff:	c3                   	ret    

00000300 <chdir>:
 300:	b8 09 00 00 00       	mov    $0x9,%eax
 305:	cd 40                	int    $0x40
 307:	c3                   	ret    

00000308 <dup>:
 308:	b8 0a 00 00 00       	mov    $0xa,%eax
 30d:	cd 40                	int    $0x40
 30f:	c3                   	ret    

00000310 <getpid>:
 310:	b8 0b 00 00 00       	mov    $0xb,%eax
 315:	cd 40                	int    $0x40
 317:	c3                   	ret    

00000318 <sbrk>:
 318:	b8 0c 00 00 00       	mov    $0xc,%eax
 31d:	cd 40                	int    $0x40
 31f:	c3                   	ret    

00000320 <sleep>:
 320:	b8 0d 00 00 00       	mov    $0xd,%eax
 325:	cd 40                	int    $0x40
 327:	c3                   	ret    

00000328 <uptime>:
 328:	b8 0e 00 00 00       	mov    $0xe,%eax
 32d:	cd 40                	int    $0x40
 32f:	c3                   	ret    

00000330 <putc>:
 330:	55                   	push   %ebp
 331:	89 e5                	mov    %esp,%ebp
 333:	83 ec 28             	sub    $0x28,%esp
 336:	8b 45 0c             	mov    0xc(%ebp),%eax
 339:	88 45 f4             	mov    %al,-0xc(%ebp)
 33c:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 343:	00 
 344:	8d 45 f4             	lea    -0xc(%ebp),%eax
 347:	89 44 24 04          	mov    %eax,0x4(%esp)
 34b:	8b 45 08             	mov    0x8(%ebp),%eax
 34e:	89 04 24             	mov    %eax,(%esp)
 351:	e8 5a ff ff ff       	call   2b0 <write>
 356:	c9                   	leave  
 357:	c3                   	ret    

00000358 <printint>:
 358:	55                   	push   %ebp
 359:	89 e5                	mov    %esp,%ebp
 35b:	53                   	push   %ebx
 35c:	83 ec 44             	sub    $0x44,%esp
 35f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 366:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 36a:	74 17                	je     383 <printint+0x2b>
 36c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 370:	79 11                	jns    383 <printint+0x2b>
 372:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
 379:	8b 45 0c             	mov    0xc(%ebp),%eax
 37c:	f7 d8                	neg    %eax
 37e:	89 45 f4             	mov    %eax,-0xc(%ebp)
 381:	eb 06                	jmp    389 <printint+0x31>
 383:	8b 45 0c             	mov    0xc(%ebp),%eax
 386:	89 45 f4             	mov    %eax,-0xc(%ebp)
 389:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 390:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 393:	8b 5d 10             	mov    0x10(%ebp),%ebx
 396:	8b 45 f4             	mov    -0xc(%ebp),%eax
 399:	ba 00 00 00 00       	mov    $0x0,%edx
 39e:	f7 f3                	div    %ebx
 3a0:	89 d0                	mov    %edx,%eax
 3a2:	0f b6 80 ec 07 00 00 	movzbl 0x7ec(%eax),%eax
 3a9:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 3ad:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 3b1:	8b 45 10             	mov    0x10(%ebp),%eax
 3b4:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 3b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 3ba:	ba 00 00 00 00       	mov    $0x0,%edx
 3bf:	f7 75 d4             	divl   -0x2c(%ebp)
 3c2:	89 45 f4             	mov    %eax,-0xc(%ebp)
 3c5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 3c9:	75 c5                	jne    390 <printint+0x38>
 3cb:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 3cf:	74 2a                	je     3fb <printint+0xa3>
 3d1:	8b 45 ec             	mov    -0x14(%ebp),%eax
 3d4:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 3d9:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 3dd:	eb 1d                	jmp    3fc <printint+0xa4>
 3df:	8b 45 ec             	mov    -0x14(%ebp),%eax
 3e2:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 3e7:	0f be c0             	movsbl %al,%eax
 3ea:	89 44 24 04          	mov    %eax,0x4(%esp)
 3ee:	8b 45 08             	mov    0x8(%ebp),%eax
 3f1:	89 04 24             	mov    %eax,(%esp)
 3f4:	e8 37 ff ff ff       	call   330 <putc>
 3f9:	eb 01                	jmp    3fc <printint+0xa4>
 3fb:	90                   	nop
 3fc:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 400:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 404:	79 d9                	jns    3df <printint+0x87>
 406:	83 c4 44             	add    $0x44,%esp
 409:	5b                   	pop    %ebx
 40a:	5d                   	pop    %ebp
 40b:	c3                   	ret    

0000040c <printf>:
 40c:	55                   	push   %ebp
 40d:	89 e5                	mov    %esp,%ebp
 40f:	83 ec 38             	sub    $0x38,%esp
 412:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 419:	8d 45 0c             	lea    0xc(%ebp),%eax
 41c:	83 c0 04             	add    $0x4,%eax
 41f:	89 45 f4             	mov    %eax,-0xc(%ebp)
 422:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 429:	e9 7e 01 00 00       	jmp    5ac <printf+0x1a0>
 42e:	8b 55 0c             	mov    0xc(%ebp),%edx
 431:	8b 45 ec             	mov    -0x14(%ebp),%eax
 434:	8d 04 02             	lea    (%edx,%eax,1),%eax
 437:	0f b6 00             	movzbl (%eax),%eax
 43a:	0f be c0             	movsbl %al,%eax
 43d:	25 ff 00 00 00       	and    $0xff,%eax
 442:	89 45 e8             	mov    %eax,-0x18(%ebp)
 445:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 449:	75 2c                	jne    477 <printf+0x6b>
 44b:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 44f:	75 0c                	jne    45d <printf+0x51>
 451:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 458:	e9 4b 01 00 00       	jmp    5a8 <printf+0x19c>
 45d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 460:	0f be c0             	movsbl %al,%eax
 463:	89 44 24 04          	mov    %eax,0x4(%esp)
 467:	8b 45 08             	mov    0x8(%ebp),%eax
 46a:	89 04 24             	mov    %eax,(%esp)
 46d:	e8 be fe ff ff       	call   330 <putc>
 472:	e9 31 01 00 00       	jmp    5a8 <printf+0x19c>
 477:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 47b:	0f 85 27 01 00 00    	jne    5a8 <printf+0x19c>
 481:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 485:	75 2d                	jne    4b4 <printf+0xa8>
 487:	8b 45 f4             	mov    -0xc(%ebp),%eax
 48a:	8b 00                	mov    (%eax),%eax
 48c:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 493:	00 
 494:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 49b:	00 
 49c:	89 44 24 04          	mov    %eax,0x4(%esp)
 4a0:	8b 45 08             	mov    0x8(%ebp),%eax
 4a3:	89 04 24             	mov    %eax,(%esp)
 4a6:	e8 ad fe ff ff       	call   358 <printint>
 4ab:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 4af:	e9 ed 00 00 00       	jmp    5a1 <printf+0x195>
 4b4:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 4b8:	74 06                	je     4c0 <printf+0xb4>
 4ba:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 4be:	75 2d                	jne    4ed <printf+0xe1>
 4c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4c3:	8b 00                	mov    (%eax),%eax
 4c5:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 4cc:	00 
 4cd:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 4d4:	00 
 4d5:	89 44 24 04          	mov    %eax,0x4(%esp)
 4d9:	8b 45 08             	mov    0x8(%ebp),%eax
 4dc:	89 04 24             	mov    %eax,(%esp)
 4df:	e8 74 fe ff ff       	call   358 <printint>
 4e4:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 4e8:	e9 b4 00 00 00       	jmp    5a1 <printf+0x195>
 4ed:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 4f1:	75 46                	jne    539 <printf+0x12d>
 4f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4f6:	8b 00                	mov    (%eax),%eax
 4f8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
 4fb:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 4ff:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 503:	75 27                	jne    52c <printf+0x120>
 505:	c7 45 e4 e5 07 00 00 	movl   $0x7e5,-0x1c(%ebp)
 50c:	eb 1f                	jmp    52d <printf+0x121>
 50e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 511:	0f b6 00             	movzbl (%eax),%eax
 514:	0f be c0             	movsbl %al,%eax
 517:	89 44 24 04          	mov    %eax,0x4(%esp)
 51b:	8b 45 08             	mov    0x8(%ebp),%eax
 51e:	89 04 24             	mov    %eax,(%esp)
 521:	e8 0a fe ff ff       	call   330 <putc>
 526:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 52a:	eb 01                	jmp    52d <printf+0x121>
 52c:	90                   	nop
 52d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 530:	0f b6 00             	movzbl (%eax),%eax
 533:	84 c0                	test   %al,%al
 535:	75 d7                	jne    50e <printf+0x102>
 537:	eb 68                	jmp    5a1 <printf+0x195>
 539:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 53d:	75 1d                	jne    55c <printf+0x150>
 53f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 542:	8b 00                	mov    (%eax),%eax
 544:	0f be c0             	movsbl %al,%eax
 547:	89 44 24 04          	mov    %eax,0x4(%esp)
 54b:	8b 45 08             	mov    0x8(%ebp),%eax
 54e:	89 04 24             	mov    %eax,(%esp)
 551:	e8 da fd ff ff       	call   330 <putc>
 556:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 55a:	eb 45                	jmp    5a1 <printf+0x195>
 55c:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 560:	75 17                	jne    579 <printf+0x16d>
 562:	8b 45 e8             	mov    -0x18(%ebp),%eax
 565:	0f be c0             	movsbl %al,%eax
 568:	89 44 24 04          	mov    %eax,0x4(%esp)
 56c:	8b 45 08             	mov    0x8(%ebp),%eax
 56f:	89 04 24             	mov    %eax,(%esp)
 572:	e8 b9 fd ff ff       	call   330 <putc>
 577:	eb 28                	jmp    5a1 <printf+0x195>
 579:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 580:	00 
 581:	8b 45 08             	mov    0x8(%ebp),%eax
 584:	89 04 24             	mov    %eax,(%esp)
 587:	e8 a4 fd ff ff       	call   330 <putc>
 58c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 58f:	0f be c0             	movsbl %al,%eax
 592:	89 44 24 04          	mov    %eax,0x4(%esp)
 596:	8b 45 08             	mov    0x8(%ebp),%eax
 599:	89 04 24             	mov    %eax,(%esp)
 59c:	e8 8f fd ff ff       	call   330 <putc>
 5a1:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 5a8:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 5ac:	8b 55 0c             	mov    0xc(%ebp),%edx
 5af:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5b2:	8d 04 02             	lea    (%edx,%eax,1),%eax
 5b5:	0f b6 00             	movzbl (%eax),%eax
 5b8:	84 c0                	test   %al,%al
 5ba:	0f 85 6e fe ff ff    	jne    42e <printf+0x22>
 5c0:	c9                   	leave  
 5c1:	c3                   	ret    
 5c2:	90                   	nop
 5c3:	90                   	nop

000005c4 <free>:
 5c4:	55                   	push   %ebp
 5c5:	89 e5                	mov    %esp,%ebp
 5c7:	83 ec 10             	sub    $0x10,%esp
 5ca:	8b 45 08             	mov    0x8(%ebp),%eax
 5cd:	83 e8 08             	sub    $0x8,%eax
 5d0:	89 45 f8             	mov    %eax,-0x8(%ebp)
 5d3:	a1 08 08 00 00       	mov    0x808,%eax
 5d8:	89 45 fc             	mov    %eax,-0x4(%ebp)
 5db:	eb 24                	jmp    601 <free+0x3d>
 5dd:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5e0:	8b 00                	mov    (%eax),%eax
 5e2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 5e5:	77 12                	ja     5f9 <free+0x35>
 5e7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 5ea:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 5ed:	77 24                	ja     613 <free+0x4f>
 5ef:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5f2:	8b 00                	mov    (%eax),%eax
 5f4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 5f7:	77 1a                	ja     613 <free+0x4f>
 5f9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 5fc:	8b 00                	mov    (%eax),%eax
 5fe:	89 45 fc             	mov    %eax,-0x4(%ebp)
 601:	8b 45 f8             	mov    -0x8(%ebp),%eax
 604:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 607:	76 d4                	jbe    5dd <free+0x19>
 609:	8b 45 fc             	mov    -0x4(%ebp),%eax
 60c:	8b 00                	mov    (%eax),%eax
 60e:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 611:	76 ca                	jbe    5dd <free+0x19>
 613:	8b 45 f8             	mov    -0x8(%ebp),%eax
 616:	8b 40 04             	mov    0x4(%eax),%eax
 619:	c1 e0 03             	shl    $0x3,%eax
 61c:	89 c2                	mov    %eax,%edx
 61e:	03 55 f8             	add    -0x8(%ebp),%edx
 621:	8b 45 fc             	mov    -0x4(%ebp),%eax
 624:	8b 00                	mov    (%eax),%eax
 626:	39 c2                	cmp    %eax,%edx
 628:	75 24                	jne    64e <free+0x8a>
 62a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 62d:	8b 50 04             	mov    0x4(%eax),%edx
 630:	8b 45 fc             	mov    -0x4(%ebp),%eax
 633:	8b 00                	mov    (%eax),%eax
 635:	8b 40 04             	mov    0x4(%eax),%eax
 638:	01 c2                	add    %eax,%edx
 63a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 63d:	89 50 04             	mov    %edx,0x4(%eax)
 640:	8b 45 fc             	mov    -0x4(%ebp),%eax
 643:	8b 00                	mov    (%eax),%eax
 645:	8b 10                	mov    (%eax),%edx
 647:	8b 45 f8             	mov    -0x8(%ebp),%eax
 64a:	89 10                	mov    %edx,(%eax)
 64c:	eb 0a                	jmp    658 <free+0x94>
 64e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 651:	8b 10                	mov    (%eax),%edx
 653:	8b 45 f8             	mov    -0x8(%ebp),%eax
 656:	89 10                	mov    %edx,(%eax)
 658:	8b 45 fc             	mov    -0x4(%ebp),%eax
 65b:	8b 40 04             	mov    0x4(%eax),%eax
 65e:	c1 e0 03             	shl    $0x3,%eax
 661:	03 45 fc             	add    -0x4(%ebp),%eax
 664:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 667:	75 20                	jne    689 <free+0xc5>
 669:	8b 45 fc             	mov    -0x4(%ebp),%eax
 66c:	8b 50 04             	mov    0x4(%eax),%edx
 66f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 672:	8b 40 04             	mov    0x4(%eax),%eax
 675:	01 c2                	add    %eax,%edx
 677:	8b 45 fc             	mov    -0x4(%ebp),%eax
 67a:	89 50 04             	mov    %edx,0x4(%eax)
 67d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 680:	8b 10                	mov    (%eax),%edx
 682:	8b 45 fc             	mov    -0x4(%ebp),%eax
 685:	89 10                	mov    %edx,(%eax)
 687:	eb 08                	jmp    691 <free+0xcd>
 689:	8b 45 fc             	mov    -0x4(%ebp),%eax
 68c:	8b 55 f8             	mov    -0x8(%ebp),%edx
 68f:	89 10                	mov    %edx,(%eax)
 691:	8b 45 fc             	mov    -0x4(%ebp),%eax
 694:	a3 08 08 00 00       	mov    %eax,0x808
 699:	c9                   	leave  
 69a:	c3                   	ret    

0000069b <morecore>:
 69b:	55                   	push   %ebp
 69c:	89 e5                	mov    %esp,%ebp
 69e:	83 ec 28             	sub    $0x28,%esp
 6a1:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 6a8:	77 07                	ja     6b1 <morecore+0x16>
 6aa:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
 6b1:	8b 45 08             	mov    0x8(%ebp),%eax
 6b4:	c1 e0 03             	shl    $0x3,%eax
 6b7:	89 04 24             	mov    %eax,(%esp)
 6ba:	e8 59 fc ff ff       	call   318 <sbrk>
 6bf:	89 45 f0             	mov    %eax,-0x10(%ebp)
 6c2:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 6c6:	75 07                	jne    6cf <morecore+0x34>
 6c8:	b8 00 00 00 00       	mov    $0x0,%eax
 6cd:	eb 22                	jmp    6f1 <morecore+0x56>
 6cf:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6d2:	89 45 f4             	mov    %eax,-0xc(%ebp)
 6d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6d8:	8b 55 08             	mov    0x8(%ebp),%edx
 6db:	89 50 04             	mov    %edx,0x4(%eax)
 6de:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6e1:	83 c0 08             	add    $0x8,%eax
 6e4:	89 04 24             	mov    %eax,(%esp)
 6e7:	e8 d8 fe ff ff       	call   5c4 <free>
 6ec:	a1 08 08 00 00       	mov    0x808,%eax
 6f1:	c9                   	leave  
 6f2:	c3                   	ret    

000006f3 <malloc>:
 6f3:	55                   	push   %ebp
 6f4:	89 e5                	mov    %esp,%ebp
 6f6:	83 ec 28             	sub    $0x28,%esp
 6f9:	8b 45 08             	mov    0x8(%ebp),%eax
 6fc:	83 c0 07             	add    $0x7,%eax
 6ff:	c1 e8 03             	shr    $0x3,%eax
 702:	83 c0 01             	add    $0x1,%eax
 705:	89 45 f4             	mov    %eax,-0xc(%ebp)
 708:	a1 08 08 00 00       	mov    0x808,%eax
 70d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 710:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 714:	75 23                	jne    739 <malloc+0x46>
 716:	c7 45 f0 00 08 00 00 	movl   $0x800,-0x10(%ebp)
 71d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 720:	a3 08 08 00 00       	mov    %eax,0x808
 725:	a1 08 08 00 00       	mov    0x808,%eax
 72a:	a3 00 08 00 00       	mov    %eax,0x800
 72f:	c7 05 04 08 00 00 00 	movl   $0x0,0x804
 736:	00 00 00 
 739:	8b 45 f0             	mov    -0x10(%ebp),%eax
 73c:	8b 00                	mov    (%eax),%eax
 73e:	89 45 ec             	mov    %eax,-0x14(%ebp)
 741:	8b 45 ec             	mov    -0x14(%ebp),%eax
 744:	8b 40 04             	mov    0x4(%eax),%eax
 747:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 74a:	72 4d                	jb     799 <malloc+0xa6>
 74c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 74f:	8b 40 04             	mov    0x4(%eax),%eax
 752:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 755:	75 0c                	jne    763 <malloc+0x70>
 757:	8b 45 ec             	mov    -0x14(%ebp),%eax
 75a:	8b 10                	mov    (%eax),%edx
 75c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 75f:	89 10                	mov    %edx,(%eax)
 761:	eb 26                	jmp    789 <malloc+0x96>
 763:	8b 45 ec             	mov    -0x14(%ebp),%eax
 766:	8b 40 04             	mov    0x4(%eax),%eax
 769:	89 c2                	mov    %eax,%edx
 76b:	2b 55 f4             	sub    -0xc(%ebp),%edx
 76e:	8b 45 ec             	mov    -0x14(%ebp),%eax
 771:	89 50 04             	mov    %edx,0x4(%eax)
 774:	8b 45 ec             	mov    -0x14(%ebp),%eax
 777:	8b 40 04             	mov    0x4(%eax),%eax
 77a:	c1 e0 03             	shl    $0x3,%eax
 77d:	01 45 ec             	add    %eax,-0x14(%ebp)
 780:	8b 45 ec             	mov    -0x14(%ebp),%eax
 783:	8b 55 f4             	mov    -0xc(%ebp),%edx
 786:	89 50 04             	mov    %edx,0x4(%eax)
 789:	8b 45 f0             	mov    -0x10(%ebp),%eax
 78c:	a3 08 08 00 00       	mov    %eax,0x808
 791:	8b 45 ec             	mov    -0x14(%ebp),%eax
 794:	83 c0 08             	add    $0x8,%eax
 797:	eb 38                	jmp    7d1 <malloc+0xde>
 799:	a1 08 08 00 00       	mov    0x808,%eax
 79e:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 7a1:	75 1b                	jne    7be <malloc+0xcb>
 7a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7a6:	89 04 24             	mov    %eax,(%esp)
 7a9:	e8 ed fe ff ff       	call   69b <morecore>
 7ae:	89 45 ec             	mov    %eax,-0x14(%ebp)
 7b1:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 7b5:	75 07                	jne    7be <malloc+0xcb>
 7b7:	b8 00 00 00 00       	mov    $0x0,%eax
 7bc:	eb 13                	jmp    7d1 <malloc+0xde>
 7be:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7c1:	89 45 f0             	mov    %eax,-0x10(%ebp)
 7c4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7c7:	8b 00                	mov    (%eax),%eax
 7c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
 7cc:	e9 70 ff ff ff       	jmp    741 <malloc+0x4e>
 7d1:	c9                   	leave  
 7d2:	c3                   	ret    
